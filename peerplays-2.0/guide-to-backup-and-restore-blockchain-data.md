# Guide to backup and restore Blockchain data

## Introduction

The purpose of this document is to guide the user through the process of creating a backup and restoring blockchain data using Docker containers. In the below procedure, the "**discovery-archive-node-b**" container is focused which contains the entire blockchain data. The automation code and its notes are stored in the [README.md](https://gitlab.com/PBSA/infrastructure/devops-misc-issues/-/blob/main/production/blockchain/README.md?ref\_type=heads) file.

{% hint style="warning" %}
The example commands in this guide assume that your project is deployed in the **`/runtime/peerplays2-testnet/`**directory. \
In case of a difference in the setup, please adjust the paths accordingly.
{% endhint %}

## Why do we need a Backup?

The blockchain technology is designed to provide decentralized, secure, and transparent record-keeping of transactions across the network. There are several reasons like data loss prevention, cybersecurity threats, disaster recovery, human errors, ensuring consistency, etc., which may be a threat to data. Thus, having a backup for blockchain data should be a fundamental practice to ensure data integrity, security, and continuity. The below steps provide a detailed explanation of backup creation and restoring blockchain data.

## **Steps to create a backup**

The procedure to create a backup involves 3 steps,

### 1. Stop the Docker Container

To optimize the RocksDB WAL journal, the first step is to restart the Docker container. After restarting, you should wait for at least 2 minutes before proceeding with the next steps. Then, stop the "**discovery-archive-node-b**" Docker container that holds the blockchain data.

{% hint style="danger" %}
Collect the finalized block height value before starting the backup and use the value as a check for successful restoration. &#x20;

**Check the section:** [https://app.gitbook.com/o/-Lpj4bCPw8iNnqCwrLCQ/s/PPsAwLpyOeItHVo4Fslw/\~/changes/6/peerplays-2.0/guide-to-backup-and-restore-blockchain-data#id-5.-checks-for-successful-restore](guide-to-backup-and-restore-blockchain-data.md#id-5.-checks-for-successful-restore)
{% endhint %}

{% code overflow="wrap" %}
```bash
/usr/local/bin/docker-compose -f  /runtime/peerplays2-testnet/docker-compose.yaml restart discovery-archive-node-b 

sleep 120

/usr/local/bin/docker-compose -f  /runtime/peerplays2-testnet/docker-compose.yaml stop discovery-archive-node-b
```
{% endcode %}



### 2. Copy Data from Docker Volume

After stopping the container, copy the data from the Docker volume using the following command:

{% code overflow="wrap" %}
```bash
tar -cjvf /tmp/blockchain_backup.tar.bz /runtime/peerplays2-testnet/docker-volumes/archive-node-b 
```
{% endcode %}

### 3. Start the container

Once the data is copied, start the "**discovery-archive-node-b**" container to resume blockchain operations.

{% code overflow="wrap" %}
```bash
/usr/local/bin/docker-compose -f /runtime/peerplays2-testnet/docker-compose.yaml up -d discovery-archive-node-b
```
{% endcode %}

## Restoring from Backup

Restoring from a backup entails restoring the system or data to a previous state (state before the issue) by using a backup copy of the data.

## Steps to restore from backup

The procedure to restore from backup is explained below:

### 1. Stop the Running Blockchain Service

Before restoring, make sure to stop the blockchain service that is currently running. Run the below command on **each node**:

{% code overflow="wrap" %}
```bash
/usr/local/bin/docker-compose -f /runtime/peerplays2-testnet/docker-compose.yaml down
```
{% endcode %}

### 2. Delete Docker Volumes (Validators and Archive Nodes)

If necessary, delete all Docker volumes associated with validators and archive nodes. If required, create a backup of these volumes before deleting them (Follow the steps above to create any backup).

{% code overflow="wrap" %}
```bash
rm -rf /runtime/peerplays2-testnet/docker-volumes
```
{% endcode %}

{% hint style="info" %}
When restoring from scratch, docker volumes related to Validators and Archive Nodes required to be empty.&#x20;

**Example:** \
**I**f there is a existing chain with data and want to restore from backup, then the data need to be deleted before restoring operation.

As long as the chain is not running and there is no data present, it is not necessary to delete the docker volume.
{% endhint %}

### 3. Restore Data from Backup created on `discovery-archive-node-b` to docker volume of `discovery-validator-alice`

Copy the previously saved blockchain data from the Backup to the Docker volume directory for the "**discovery-validator-alice**" node.&#x20;

The copy command used in the backup section also can be used here.

{% code overflow="wrap" %}
```bash
# run on server with discovery-validator-alice

tar -xjvf tmp/blockchain_backup.tar.bz -C /
mv /runtime/peerplays2-testnet/docker-volumes/archive-node-b /runtime/peerplays2-testnet/docker-volumes/alice
```
{% endcode %}

### **4. Start the Blockchain Service**

After restoring the data, start the blockchain service again.&#x20;

{% code overflow="wrap" %}
```bash
# Run on each node with the related docker container name 

/usr/local/bin/docker-compose -f /runtime/peerplays2-testnet/docker-compose.yaml up -d "specify docker container"
```
{% endcode %}

The above steps can be followed to create a backup for the blockchain data, and the data can be restored if necessary.

### 5. Checks for Successful Restore&#x20;

It is important to collect the **finalized block height** number before starting the backup operation and after restoring from the backup. Compare the two values and it should be identical which will ensure a successful restoration.

There are two ways to find the finalized block height value. Use the below commands to get the number of blocks and use the values for comparison.

#### **Command 1:**

{% code overflow="wrap" %}
```bash
curl -sS -H "Content-Type: application/json" -d '{"id":1, "jsonrpc":"2.0", "method": "chain_getHeader"}' http://localhost:9944/ | jq '.result.number' | xargs printf '%d\n'
```
{% endcode %}

{% hint style="info" %}
http://localhost:9944 -> Replace it with the address and port of the running node
{% endhint %}

**Output:**

The output will be just the number of finalized blocks.

{% code overflow="wrap" %}
```bash
2022
```
{% endcode %}

#### **Command 2:**

```
docker logs "container name"
```

**Example:**

{% code overflow="wrap" %}
```bash
docker logs discovery-validator-alice 
```
{% endcode %}

**Extracted Output for reference:**

{% code overflow="wrap" %}
```bash
2024-01-16 15:07:48 💤 Idle (10 peers), best: #2025 (0x08c3…8370), finalized #2022 (0xe1d3…c408), ⬇ 19.8kiB/s ⬆ 19.8kiB/s
```
{% endcode %}

Follow these steps carefully to ensure the integrity of your blockchain data.
