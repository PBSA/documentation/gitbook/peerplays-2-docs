# Deployment in a box

## A. Introduction

This document explains about the steps involved in Peerplays2.0 deployment in a box. The environment provides all the tools to quickly start Peerplays2.0 envionrment.  Deployment in a box consists of a set of docker images which are self contained, ready to install procedure. The purpose of this document is provide a user with simple step-by-step procedure to create an end-to-end environment to test Peerplays 2.0 using docker.

## B. Features

The software used to run Peerplays2.0 is the latest docker image from the master branch in the discovery repository [https://gitlab.com/PBSA/Peerplays\_2.0/discovery/-/blob/master/Dockerfile](https://gitlab.com/PBSA/Peerplays\_2.0/discovery/-/blob/master/Dockerfile)

The following details are the list of aspects in this Test environment,

* **Peerplays 2.0 Private Testnet Multi Node network** - 11 Peerplays 2.0 nodes, running 11 validators.
* Private Docker network used for connecting all containers.
* Polkadot UI, customized for Peerplays 2.0.
* Contracts UI, to handle Ink contracts.
* The start script for Funded **substrate** accounts will point to the below endpoints,\
  **For dev:** Alice\
  **For testnet:** init00, init01, init02, init03, init04, init05, init06, init07, init08, init09, init10\
  **For mainnet:** init00, init01, init02, init03, init04, init05, init06, init07, init08, init09, init10
* The start script for  Funded **ETH** accounts will point to the below endpoints,\
  **For dev:** Alice, Alith\
  **For testnet:** init00\
  **For mainnet:** init00

## C. Software Requirements

* [ ] Officially supported Operating System - Ubuntu 20.04 OS (always use the latest version)
* [ ] Docker-compose version 1.25.0&#x20;

{% hint style="danger" %}
The testing and deployment of Peerplays 2.0 components are performed only in the Ubuntu environment.

If the user wish to test in the other environments such as Windows or MacOS, there might be a build failure. The solution to such cases are out of scope at the moment and user is on their own to find the possible solution.
{% endhint %}

## D. Steps to Deploy Peerplays 2.0 in a box <a href="#user-content-building" id="user-content-building"></a>

Follow the steps from the README file linked below to build Peerplays 2.0 hassle free,

{% embed url="https://gitlab.com/PBSA/Peerplays_2.0/discovery/-/tree/master/discovery-qa" %}
Detailed steps to build Peerplays2.0
{% endembed %}

The readme file provides the updated steps around the clock and the deployment can be done without any hindrance. However, the brief description for each step is provided below,

### 1. Build the image <a href="#user-content-building" id="user-content-building"></a>

The image can be build either fully or partially based on user's option. The **Full build** image will use a single command to build 11 images at a time. while, the **Partial build** option allows user to build one image at a time. Both build procedure involves docker commands. Refer the [README](https://gitlab.com/PBSA/Peerplays\_2.0/discovery/-/blob/master/discovery-qa/README.md#building) file for the commands.

### 2. Run a custom image <a href="#user-content-running-a-custom-image" id="user-content-running-a-custom-image"></a>

The QA environment facilitates the user with an option to use custom image as **`discovery-base`** image. In order to use a custom discovery image, that image has to be published in container registry. The dockerized job has to be run in the MR pipeline manually, to publish a docker image. The image will be published based on branch name and commit SHA in the below location,

{% embed url="https://gitlab.com/PBSA/Peerplays_2.0/discovery/container_registry" %}

Refer to the steps in the [README](https://gitlab.com/PBSA/Peerplays\_2.0/discovery/-/blob/master/discovery-qa/README.md#running-a-custom-image) file to complete the rebuild procedure.

### 3. Start the nodes <a href="#user-content-starting-nodes" id="user-content-starting-nodes"></a>

There are two options to start the node, either a full start or a partial start based on the user's choice.&#x20;

* Full start, will start all the 11 node with a single command.
* Partial start, will allow the user to start the 11 node one-by-one using unique commands.

Refer the [README](https://gitlab.com/PBSA/Peerplays\_2.0/discovery/-/blob/master/discovery-qa/README.md#starting-nodes) file to start the nodes.

### 4. Block confirmation  <a href="#user-content-confirmation" id="user-content-confirmation"></a>

As per the criteria, more than 8 nodes has to started and stay active to successfully communicate with each other. Only after the communication, the user will be able to see the finalized blocks.&#x20;

An example showing the finalized block is given below:

{% code overflow="wrap" %}
```docker
discovery-validator-grace_1    | 2023-05-03 10:51:07 💤 Idle (10 peers), best: #5 (0x52a5…77cd), finalized #3 (0xff76…a8fc), ⬇ 54.3kiB/s ⬆ 50.8kiB/s
```
{% endcode %}

### 5. Stop the nodes <a href="#user-content-stopping-nodes" id="user-content-stopping-nodes"></a>

Nodes can be stopped using a single command line. Refer the [README](https://gitlab.com/PBSA/Peerplays\_2.0/discovery/-/blob/master/discovery-qa/README.md#stopping-nodes) file to check the command.
