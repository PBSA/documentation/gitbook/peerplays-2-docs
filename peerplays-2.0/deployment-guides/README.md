# Deployment Guides

This section of the documentation provides steps deploy Peerplays2.0 and tools required to operate the blockchain as well as support tools like block explorers.

{% content-ref url="deployment-in-a-box.md" %}
[deployment-in-a-box.md](deployment-in-a-box.md)
{% endcontent-ref %}

{% content-ref url="blockscout-explorer-deployment/" %}
[blockscout-explorer-deployment](blockscout-explorer-deployment/)
{% endcontent-ref %}

{% content-ref url="../solidity-smart-contracts/" %}
[solidity-smart-contracts](../solidity-smart-contracts/)
{% endcontent-ref %}

{% content-ref url="../solidity-smart-contracts/solidity-smart-contract-deployment-using-hardhat.md" %}
[solidity-smart-contract-deployment-using-hardhat.md](../solidity-smart-contracts/solidity-smart-contract-deployment-using-hardhat.md)
{% endcontent-ref %}

{% content-ref url="../solidity-smart-contracts/solidity-smart-contract-deployment-using-remix-ide.md" %}
[solidity-smart-contract-deployment-using-remix-ide.md](../solidity-smart-contracts/solidity-smart-contract-deployment-using-remix-ide.md)
{% endcontent-ref %}

{% content-ref url="../solidity-smart-contracts/metamask-deployment.md" %}
[metamask-deployment.md](../solidity-smart-contracts/metamask-deployment.md)
{% endcontent-ref %}
