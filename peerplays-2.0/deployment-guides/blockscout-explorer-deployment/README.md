---
description: Blockchain explorer for examining and inspecting EVM chains.
---

# Blockscout Explorer Deployment

## A. Introduction

Blockscout is a fully-featured open-source blockchain explorer which can be used by any Ethereum virtual machine (EVM) chain. Blockscout provide access to essential on-chain data, the ability to interact with smart contracts, and tools to analyse any transactions at ease. The major features of blockscout are multichain, highly configurable, and open source. The explorer allows the user to search blocks, transaction, addresses and also connect & interact with smart contract, application, bridges, wallets, etc.,

## B. Hardware Requirements

The hardware requirement details to install the explorer for **Peerplays2.0** is provided below.

### 1. Recommended Base Hardware (Provided by Blockscout)

EVM chains can differ in size and requirements, these are the recommendations for optimal performance.

| CPU | 16 core, 32 thread |
| --- | ------------------ |
| RAM | 128GB              |

### 2. Currently Deployed Hardware For Peerplays2

| CPU  | 8         |
| ---- | --------- |
| RAM  | 16GB      |
| Disk | 160GB SSD |

### 3. Current Hardware Utilized Connected To Peerplays2

| CPU                            | 8 Core/Thread Virtualized |
| ------------------------------ | ------------------------- |
| RAM                            | 16GB                      |
| Disk                           | 15GB                      |
| CPU Average (1, 5, 15 Minutes) | 0.25, 0.11, 0.10          |
| RAM Usage                      | 5.95GB                    |
| Disk Usage                     | 25GB                      |

## C. Software Requirements

Before installing the Blockscout explorer, the user must ensure that the below required software are installed in their machine. Follow the CLI below to install the desired software,

### 1. Docker

Docker is an open software platform that enables the user to build, deploy, test, and update applications swiftly. Docker uses containers for all the execution. A container is a total software package of an application's code, its libraries and other dependencies. The main advantage of using container is that, our application is portable and the same code can be executed on any device. The cli to install docker is provided below:

{% code overflow="wrap" %}
```sh
curl -fsSL [<https://get.docker.com>](<https://get.docker.com/>) -o [install-docker.sh](<http://install-docker.sh/>)

sudo sh install-docker.sh
```
{% endcode %}

### 2. Erlang

Erlang is used to build massive scalable soft real-time system with requirements on high availability. Install using the below command:

```bash
sudo apt install erlang
```

### 3. Elixir

Elixir is a dynamic, functional language for building scalable and maintainable applications. Elixir runs on the Erlang VM, known for creating low-latency, distributed, and fault-tolerant systems. Install the Elixir using the below command:

```bash
sudo add-apt-repository ppa:rabbitmq/rabbitmq-erlang
sudo apt update
sudo apt install elixir
```

### 4. PostgreSQL 14

PostgreSQL is an open source relational database management system emphasizing extensibility and SQL compliance. It is the primary primary datastore or data warehouse for  many web, mobile, geospatial, and analytics application. Install PostgreSQL 14 using the below CLI:

{% code overflow="wrap" %}
```bash
sudo sh -c 'echo "deb <http://apt.postgresql.org/pub/repos/apt> $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
wget --quiet -O - <https://www.postgresql.org/media/keys/ACCC4CF8.asc> | sudo apt-key add -
sudo apt update
sudo apt install postgresql-14
```
{% endcode %}

### 5. Node 18.x

Node 18 execute the JavaScript using the updated version to  boost the performance of web applications. It allows to create and execute tests without intervention from any third party package before adding code to action. Its the build-in node. Install latest Node 18 version using the below commands:

{% code overflow="wrap" %}
```bash
curl -fsSL <https://deb.nodesource.com/setup_18.x> | sudo -E bash -
sudo apt-get install -y nodejs
```
{% endcode %}

#### 6. Apt dependencies

The apt dependencies are the supporting packages required to run the application in Linux without interruption. Install the necessary package using below command:

```bash
sudo apt install automake libtool inotify-tools libgmp-dev make g++
```

## **D. Setting up the application**

In the terminal, follow the step-by-step procedure given below to setup the application

1. **Grab the repository**\
   Clone the repository from the GitHub.

```bash
git clone <https://github.com/blockscout/blockscout> && cd blockscout
```

2. **Ensure connectivity to PostgreSQL**\
   Login the PSQL using the correct database user.

```bash
sudo --login --user=postgres psql
```

3. **Export PostgreSQL database URL**\
   Export the database to blockscout.

```bash
export DATABASE_URL=postgresql://user:password@localhost:5432/blockscout
```

4. **Install Mix dependencies and compile them**

```bash
mix do deps.get, local.rebar --force, deps.compile
```

5. **Generate a new secret\_key\_base for the DB**\
   Using the below command, generate the key for database:

```bash
mix phx.gen.secret
```

6. **Export the secret key**

```bash
export SECRET_KEY_BASE=<output-of-phx.gen.secret>
```

7. **Set the following environment variables specific to Peerplays2.0**\
   The below environment details are specific to Peerplays2.0 application:

```bash
 export COIN_NAME=PPY-Test && \\
 export RUST_VERIFICATION_SERVICE_URL=http://0.0.0.0:8043/ && \\
 export MIX_ENV="prod" && \\
 export MICROSERVICE_SC_VERIFIER_ENABLED=true && \\
 export ETHEREUM_JSONRPC_HTTP_URL=http://<IP-OF-YOUR-SERVER>:9944 && \\
 export ETHEREUM_JSONRPC_TRACE_URL=http://<IP-OF-YOUR-SERVER>:9944 && \\
 export ETHEREUM_JSONRPC_TRACE_URL=http://<IP-OF-YOUR-SERVER>:9944 && \\
 export ETHEREUM_JSONRPC_WS_URL=ws://<IP-OF-YOUR-SERVER>:9944 && \\
 export ETHEREUM_JSONRPC_HTTP_INSECURE=true && \\
 export PORT=4000 && \\
 export BLOCKSCOUT_PROTOCOL=http && \\
 export POOL_SIZE=100 && \\
 export INDEXER_EMPTY_BLOCKS_SANITIZER_BATCH_SIZE=1000 && \\
 export DISPLAY_TOKEN_ICONS=true && \\
 export LINK_TO_OTHER_EXPLORERS=false && \\
 export DISABLE_EXCHANGE_RATES=true && \\
 export INDEXER_DISABLE_BLOCK_REWARD_FETCHER=true && \\
 export INDEXER_DISABLE_PENDING_TRANSACTIONS_FETCHER=true && \\
 export INDEXER_DISABLE_INTERNAL_TRANSACTIONS_FETCHER=true && \\
 export INDEXER_EMPTY_BLOCKS_SANITIZER_BATCH_SIZE=1000 && \\
 export LOGO=/images/PPYIcon.svg && \\
 export LOGO_FOOTER=/images/PPYIcon.svg && \\ 
 export NETWORK=POP Network
 export SUBNETWORK=Peerplays
```

8. **Start Smart contract verification micro-service with docker**\
   Use below command to start the docker:

```bash
docker run -p 8050:8050 ghcr.io/blockscout/smart-contract-verifier:latest
```

9. **Compile the application**\
   compile using the below command:

```bash
mix compile
```

10. &#x20;**Create and migrate database**

```bash
mix do ecto.create, ecto.migrate
```

11. **Installing node.js dependencies**\
    Install the required modules using below commands:

{% code overflow="wrap" %}
```bash
cd apps/block_scout_web/assets; 
npm install && node_modules/webpack/bin/webpack.js --mode production; 
cd -
cd apps/explorer && npm install; 
cd -
```
{% endcode %}

12. **Build static assets (if any)**

```bash
mix phx.digest
```

13. **Generate self signed certs for running locally (use certbot if deploying production)**\
    The required certificate can be generated using below commands:

{% code overflow="wrap" %}
```bash
cd apps/block_scout_web; 
mix phx.gen.cert blockscout blockscout.local; 
cd
```
{% endcode %}

14. **Start the server with tmux**\
    To have multiple sessions and to switch between, start the application with tmux.

{% code overflow="wrap" %}
```bash
tmux new -s blockscout
mix phx.server
Hold CTRL & Tap b, then d to detach from the tmux session
Reattach to tmux with - tmux attach -s blockscout
```
{% endcode %}

## E. Restoring from a PostgreSQL backup

First, create `blockscout` database dump using a **proper database user**. It's important because the application can be restored only using this data. Follow the below steps,

1. **Create a dump of the `blockscout` database using below command:**

{% code overflow="wrap" %}
```bash
pg_dump -F t blockscout > blockscout.tar
```
{% endcode %}

2. Drop the database and recreate an empty database from **`~/blockscout/`**

```
mix do ecto.drop, ecto.create
```

{% hint style="warning" %}
Restarting blockscout from the above location will result in error, as the database has not been migrated - Follow next step to restore the database.
{% endhint %}

3. **Restore the database using the dump taken earlier using below command:**

{% code overflow="wrap" %}
```bash
pg_restore --verbose --clean --no-acl -h localhost -U <db-user> -d blockscout blockscout.tar
```
{% endcode %}

The Application has to be restarted from here and it starts the indexing again from where it left off last in the database dump.
