---
description: The deployment uses Docker Compose
---

# Deploying Microservices to run Blockscout Explorer locally

## Introduction

Docker Compose is a tool that helps you define and share multi-container applications. With Compose, you can create a YAML file to define the services and with a single command, you can spin everything up or tear it all down. The document explains about the steps to run blockscout locally in Docker containers with docker compose.

Click [here](https://github.com/docker/compose), to learn about docker compose in detail.

The detailed explanation about Docker-compose configuration is explained in the below link,

{% embed url="https://github.com/blockscout/blockscout/tree/master/docker-compose#readme" %}

## The brief explanation about docker compose configuration is explained below:

The blockscout explorer can be run locally in Docker container with docker compose.

## Deployment Steps

### Prerequisites

* Docker v20.10+
* Docker-compose 2.x.x+
* Running Ethereum JSON RPC client

## Steps

1. There are two ways in building the docker,&#x20;

* [Building Docker containers from source](https://github.com/blockscout/blockscout/tree/master/docker-compose#building-docker-containers-from-source-with-native-smart-contract-verification-deprecated)
* [Building Docker container](https://github.com/blockscout/blockscout/tree/master/docker-compose#building-docker-containers-from-source-with-native-smart-contract-verification-deprecated) from source with native smart contract verification (deprecated)

2. There are different [configs available for Ethereum clients](https://github.com/blockscout/blockscout/tree/master/docker-compose#configs-for-different-ethereum-clients). The repository contains built-in configs for different clients without a need to build the image.

