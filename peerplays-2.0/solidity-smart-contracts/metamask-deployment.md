# Metamask Deployment

## Introduction

Metamask is a cryptocurrency wallet used to interact with the Ethereum blockchain. Metamask is a  non-custodial wallet, which means that the crypto owner only will hold their private key and therefore their funds. There is no third party involved in any transactions as it has the decentralized wallet. This wallet securely connects the users with different blockchain-based applications and decentralized web.&#x20;

## Benefits

1. Safe and secured wallet to manage crypto investments.
2. User has the complete access to their funds and transaction as there is no middle man involved.
3. Offers advanced security features to protect the digital assets against phishing attacks.

## Limitations

1. By default, the wallet only comes with Ethereum network and user has to add additional networks manually.
2. Metamask doesn't support Bitcoin.

## Steps to add Peerplays2.0 network in Metamask

1. Download the browser extension for MetaMask based on the choice of web browser to create a wallet. The below link direct to the MetaMask webpage,\
   [https://metamask.io/](https://metamask.io/)
2. After **successful wallet creation**, the user will be directed to MetaMask account with Ethereum Mainnet as default network.

<figure><img src="../../.gitbook/assets/Account-creation.JPG" alt=""><figcaption><p>Default account</p></figcaption></figure>

The user must add the required network to the MetaMask wallet to access it.

3. First, if the user wants to create a new account / import account / to use hardware wallet, click on the drop-down option from the account name and select necessary options.

<figure><img src="../../.gitbook/assets/Add-account (1).JPG" alt=""><figcaption><p>Adding New account</p></figcaption></figure>

4. Next, with the selected account:\
   A) Click on the dots at the top right corner and choose **Settings** option, then select **Network tab** on the left pane of settings page.

<figure><img src="../../.gitbook/assets/Settings-network.JPG" alt=""><figcaption><p>settings option</p></figcaption></figure>

Click on "**Add a network**" button to input the values.

<figure><img src="../../.gitbook/assets/Add-network-2.JPG" alt=""><figcaption><p>Add network option</p></figcaption></figure>

B) Otherwise, click the **drop down option** from the network name as highlighted below and click on **Add network** button.

<figure><img src="../../.gitbook/assets/Add-network-1.JPG" alt=""><figcaption><p>Add network option</p></figcaption></figure>

5. After selecting the "Add network" option, the following tab will be opened to input detail about the network.

<figure><img src="../../.gitbook/assets/input-details-manually.JPG" alt=""><figcaption><p>Input options</p></figcaption></figure>

6. Enter the exact values of your network manually and click on "**Save**" button to reflect the update.

<figure><img src="../../.gitbook/assets/manual-iput-details.JPG" alt=""><figcaption><p>Input values for network manually</p></figcaption></figure>

## Network Information

|       Name      |                                   Value                                  |
| :-------------: | :----------------------------------------------------------------------: |
|   Network Name  |                               Peerplays 2.0                              |
|   RPC Endpoint  |                          http://96.46.48.73:9933                         |
|     Chain ID    |                                    33                                    |
| Currency Symbol |                                   TEST                                   |
|  Block Explorer | [http://96.46.48.73:3000/#/explorer](http://96.46.48.73:3000/#/explorer) |
|      Bridge     |     [https://portfolio.metamask.io/](https://portfolio.metamask.io/)     |

7. Now, the new network is successfully added in the wallet for use.

<figure><img src="../../.gitbook/assets/nw-added-sucessfully.JPG" alt=""><figcaption><p>Network added successfully</p></figcaption></figure>

8. Click on the three dot on the top right corner of the page to explore the options available for the accounts.

<figure><img src="../../.gitbook/assets/settings.JPG" alt=""><figcaption><p>Account explore options</p></figcaption></figure>

MetaMask Deployment is successful ✅
