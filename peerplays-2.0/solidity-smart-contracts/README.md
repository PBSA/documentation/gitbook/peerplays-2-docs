# Solidity Smart contracts

## Description

A smart contract is a self executing program that automates the actions required in an agreement or contract. After the task completion, the transaction is trackable and irreversible.

## What is a Solidity Smart contract ?

A solidity smart contract provide a highly secured digital transactions which doesn't involve the third parties.

Solidity is an object-oriented programming language created specially by the Ethereum Network Team for constructing and designing smart contracts on Blockchain platforms.

For Peerplays 2.0 there are two methods adapted to deploy the smart contract,

{% content-ref url="solidity-smart-contract-deployment-using-remix-ide.md" %}
[solidity-smart-contract-deployment-using-remix-ide.md](solidity-smart-contract-deployment-using-remix-ide.md)
{% endcontent-ref %}

{% content-ref url="solidity-smart-contract-deployment-using-hardhat.md" %}
[solidity-smart-contract-deployment-using-hardhat.md](solidity-smart-contract-deployment-using-hardhat.md)
{% endcontent-ref %}
