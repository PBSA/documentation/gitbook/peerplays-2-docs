# Solidity smart contract deployment using hardhat

## Deployment Using Hardhat

There are three steps involved in the contract deployment using Hardhat,

1. Create a new Hardhat project
2. Configure Hardhat project with targeted blockchain
3. Compile and deploy

## Requirement specification

1. Visual Studio - VS code
2. Hardhat&#x20;

## Software Installation

**A) Setup the Hardhat environment in the VS code:**

Follow the step-by-step procedure explained in the tutorial provided by Hardhat,

{% embed url="https://hardhat.org/hardhat-runner/docs/guides/project-setup" %}
Guide to install Hardhat
{% endembed %}

Now, the VS code will be installed with Hardhat framework and ready to begin with smart contract deployment.

## Solidity Smart contract Deployment using Hardhat

The steps to deploy smart contract using Hardhat is explained below,

1. Login to the VS code environment which has the Hardhat framework installed with necessary files and folders to access.
2. Next, OpenZeppelin is an external library which should be present in VS code for contract deployment and it can be installed using below command:

```visual-basic
npm install @openzeppelin/contracts
```

<figure><img src="../../.gitbook/assets/VS-code-file-structure.jpg" alt=""><figcaption><p>File structure</p></figcaption></figure>

The **`TEST_ERC20.sol`** is the script provided by OpenZeppelin library to provide the desired minting value.

3. There are two files used for contract deployment&#x20;

&#x20;        **A.** hardhat\_config.ts - Config file

&#x20;        **B.** deploy\_config.ts - Deploy script

Example script structure is provided below:

The user has to provide the account's private key for **`accounts`** value in the **`hardhat_config`** file.&#x20;

{% code overflow="wrap" %}
```tsconfig
// Hardhat_config
import { HardhatUserConfig } from "hardhat/config";
import "@nomicfoundation/hardhat-toolbox";

const config: HardhatUserConfig = {
  solidity: "0.8.19",
  defaultNetwork: 'peerplays',
  networks: {
    peerplays: {
      url: 'http://96.46.48.200:9933',
      accounts: [`0x` + process.env.PRIVATE_KEY], // Account's private key is added
      chainId: 33,
    },
  }
};

export default config;
```
{% endcode %}

Ethers library is provided by Ethereum and it has the necessary functions required for calls. Deploy script is required to deploy smart contract.&#x20;

{% code overflow="wrap" %}
```tsconfig
// Deploy_config
import { ethers } from "hardhat";

async function main() {

  const myToken = await ethers.deployContract("MyToken");

  await myToken.waitForDeployment();

  console.log(`MyToken deployed to: ${myToken.target}`) // Mention the Contract name used
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
```
{% endcode %}

4. The command to deploy contract is given below:

```
npx hardhat run scripts/deploy.ts
```

<figure><img src="../../.gitbook/assets/Hardhat-deploy-image.png" alt=""><figcaption></figcaption></figure>

The contract address will be generated with deployment successful message.

Solidity smart contract deployment using Hardhat is successful! ✅
