# Solidity smart contract deployment using REMIX IDE

## Deployment using REMIX IDE

1. Open Remix IDE using the below link, [https://remix.ethereum.org/#lang=en\&optimize=false\&runs=200\&evmVersion=null\&version=soljson-v0.8.18+commit.87f61d96.js](https://remix.ethereum.org/#lang=en\&optimize=false\&runs=200\&evmVersion=null\&version=soljson-v0.8.18+commit.87f61d96.js). The default workspace looks similar like below,

<figure><img src="../../.gitbook/assets/Default-workspace.JPG" alt=""><figcaption><p>Default workspace</p></figcaption></figure>

2. A) Next step is to create a workspace, click on the **Workspace** option and select **create -> create workspace**. This will open a new tab to choose the type of workspace as required.

<figure><img src="../../.gitbook/assets/workspace-create-1.jpg" alt=""><figcaption><p>Create new workspace</p></figcaption></figure>

B) The new tab has various **Template** options to choose for the new workspace. The option selected here is **Open Zeppelin ->ERC20.** Open Zeppelin is the open source library/framework mainly used in the smart contract deployment for ethereum. It provides a set of audited and reusable contracts for the developers.

<figure><img src="../../.gitbook/assets/workspace-creation-2.jpg" alt=""><figcaption><p>Choose Template for workspace</p></figcaption></figure>

C) Click on **OK**, the next page has some customizable template options for the new workspace. Choose the required options and click **OK** to create the workspace.

<figure><img src="../../.gitbook/assets/workspace-creation-3.JPG" alt=""><figcaption><p>Template customizable options</p></figcaption></figure>

{% hint style="info" %}
The template settings can be modified anytime after the workspace creation too.&#x20;
{% endhint %}

Now, the **Workspace** is created successfully.

D) The new workspace created will be similar to the below page and it has its own files and folders.

<figure><img src="../../.gitbook/assets/Openziplin-workspace.JPG" alt=""><figcaption></figcaption></figure>

3. There is one more application called **MetaMask**, which is required to begin the smart contract deployment. If the user has an existing MetaMask wallet then it can be used or else the new account has to be created. The steps to deploy MetaMask is explained in the below section.

{% content-ref url="metamask-deployment.md" %}
[metamask-deployment.md](metamask-deployment.md)
{% endcontent-ref %}

4. Once the MetaMask configuration is completed, switch back to the REMIX IDE workspace to resume the steps.
5. Choose **Deploy & Run Transactions** tab from the left pane of the workspace. To select the **Environment** option, click on the drop-down menu to choose **Injected provider - MetaMask.**

<figure><img src="../../.gitbook/assets/Env-inject-provider.JPG" alt=""><figcaption></figcaption></figure>

6. A) Next, a new tab will prompt to connect with Metamask wallet. Choose the desired MetaMask account that has to be connected with the Remix IDE workspace.

<figure><img src="../../.gitbook/assets/connect-with-metamask.JPG" alt=""><figcaption></figcaption></figure>

B) Click on **Next** to see the below screen and provide permission for the REMIX IDE to view the wallet details. Finally, click on **Connect** to complete the environment setup.

<figure><img src="../../.gitbook/assets/connect-with-metamask-connect.JPG" alt=""><figcaption></figcaption></figure>

C) Now, the MetaMask wallet is connected with the REMIX IDE. After successful connection, the Metamask wallet address will be populated in the setup details. Also, the wallet and its balance will be visible from the IDE.&#x20;

<figure><img src="../../.gitbook/assets/Remix-metamask-connected.JPG" alt=""><figcaption></figcaption></figure>

7. Switch to OpenZeppelin workspace to begin with the smart contract deployment. From the list of folders, select **contracts** and click on **MyToken.sol** which has the default configuration.

<figure><img src="../../.gitbook/assets/work-space-token.JPG" alt=""><figcaption></figcaption></figure>

8. The contract should be modified based on the requirements. The boiler plate for **MyToken.sol** is given below:

{% code overflow="wrap" %}
````solidity
```remix-solidity
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.9;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract MyToken is ERC20 {
    constructor() ERC20("TEST_PPY_2.0", "TEST") 
    {
    _mint(msg.sender, 1000000000000000000000);
    }
}
```
````
{% endcode %}

The given example is used to mint 1000 token and based on user option the value can be modified.

{% hint style="info" %}
The token value must be followed by 18 Zero's. Because, the default setting for Ethereum minting is the values must be followed by "0"  up to 18 decimal counts.
{% endhint %}

The contract is modified and it is ready to be deployed. Click on **CTRL+S** to complete the compilation, only then the updates will be reflected.

One more option to compile is, choose **Solidity Compiler** option and select compile **MyToken.sol** button to finish compilation.

<figure><img src="../../.gitbook/assets/compile-tokens.JPG" alt=""><figcaption></figcaption></figure>

9. Next, add the contract script in the Environment value and contract deployment is one step away!

A) Switch to **Deploy & Run Environment** option, choose **MyToken - contract/MyToken.sol** option from the drop down list.

<figure><img src="../../.gitbook/assets/token-addition.jpg" alt=""><figcaption></figcaption></figure>

B) Click on **Deploy**, the MetaMask window will be prompted to complete the transaction. Click on **confirm** to finish the contract deployment.&#x20;

<figure><img src="../../.gitbook/assets/deploying-prompted.JPG" alt=""><figcaption></figcaption></figure>

10. After confirmation, the deployment process begins and finally the contract deployment will be completed. The contract address is generated and click on copy icon to have a copy of address for verification.

<figure><img src="../../.gitbook/assets/contract-deployed.JPG" alt=""><figcaption></figcaption></figure>

The contract address will be similar to the below format:

````
// contract address example
```remix-solidity
0xde1aF1323bD6d092a9199d8f050A9806bcb57530
```
````

Solidity smart contract deployment using REMIX IDE is successful! ✅
