# Howto & FAQ

The following list of commands helps the user to query the details in a node

1. Querying a node for PeerID

{% code overflow="wrap" %}
```sh
curl -H "Content-Type: application/json" -d '{"id":1, "jsonrpc":"2.0", "method": "system_localPeerId", "params":[]}' http://192.168.150.105:10933
```
{% endcode %}



2. Querying a node for block details using block hash

_Assuming that the chain is running on the localhost with default RPC port_

{% code overflow="wrap" %}
```shell
curl -H "Content-Type: application/json" -d '{"id":1, "jsonrpc":"2.0", "method": "eth_getBlockByHash", "params":["0x2f57daea521ff418d3ef3e35433264085a185c045969e1edcd1d04ad14220a3e", true]}' http://localhost:9944 | jq


```
{% endcode %}

3. Genesis mismatch

When we try to sync the chain,  we might get errors about geneis mismatch like below:

`Report 12D3KooWEyoppNCUx8Yx66oV9fJnriXwCcXwDDUA2kj6vnc6iDEp: -2147483648 to -2147483648. Reason: Genesis mismatch. Banned, disconnecting.`



This means that the genesis file of the original network is different  from the one available to the node being started. We must get the correct genesis file from the source location/chain. To generate genesis file on running node using `peerplays build-spec`

Example: `./peerplays build-spec --chain=testnet > genesis.json`

Than run node with parameter `--chain=genesis.json`

```bash
./peerplays --chain=genesis.json --base-path ./peerplays_data_dir/ --bootnodes /ip4/192.168.20.204/tcp/30333/p2p/12D3KooWEyoppNCUx8Yx66oV9fJnriXwCcXwDDUA2kj6vnc6iDEp
```



