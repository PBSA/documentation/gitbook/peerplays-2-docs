# Key Metrics

## Monitoring setup

Please refer to the substrate node guide to set up monitoring.

{% embed url="https://docs.substrate.io/tutorials/build-a-blockchain/monitor-node-metrics/" %}

## Hardware requirements

Before starting the node, check that the system has at least 2GiB of free space

During node startup pay attention to the host performance check there will be entries in the log with the current hardware score

&#x20;   🏁 CPU score:\
&#x20;   🏁 Memory score: \
&#x20;   🏁 Disk score (seq. writes): \
&#x20;   🏁 Disk score (rand. writes):&#x20;

In case of insufficient resources, there will be a message:

{% hint style="danger" %}
&#x20;⚠️ The hardware does not meet the minimal requirements for the role of 'Authority'.
{% endhint %}



## Important metrics to monitor

1. Block construction time should not be more than 5 seconds

**Metrics:**&#x20;

```
substrate_proposer_block_constructed_bucket
```

**Usage example with Prometheus:**

{% code overflow="wrap" %}
```promql
histogram_quantile(0.90, sum(rate(substrate_proposer_block_constructed_bucket[5m])) by (le))
```
{% endcode %}

2. 'Finalized' chain shouldn't lag behind 'best' for more than 10 blocks ('best' - 'finalized' < 10)

**Metrics:**

```
substrate_block_height{status="best"} 
substrate_block_height{status="finalized"}
```

**Usage example with Prometheus:**

{% code overflow="wrap" %}
```promql
sum by (name, instance) (substrate_block_height{status="best", chain="testnet"}) - sum by (name, instance)(substrate_block_height{status="finalized", chain="testnet"})
```
{% endcode %}

3. Number of connected peers should be more than 0 and depends on network size. On large networks, it will be limited by libp2p default value of 300.

**Metrics:**

```
substrate_sub_libp2p_peers_count
```



